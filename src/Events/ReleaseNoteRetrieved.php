<?php

namespace Ntriga\ReleaseNotes\Events;

use Illuminate\Http\Request;
use Ntriga\ReleaseNotes\Models\ReleaseNote;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReleaseNoteRetrieved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var ReleaseNote
     */
    public $releaseNote;

    /**
     * @var Request
     */
    public $request;

    /**
     * Create a new event instance.
     *
     * @param ReleaseNote $releaseNote
     * @param Request $request
     */
    public function __construct(ReleaseNote $releaseNote)
    {
        $this->releaseNote = $releaseNote;
        $this->request = request();
    }
}
