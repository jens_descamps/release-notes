<?php

namespace Ntriga\ReleaseNotes\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Ntriga\ReleaseNotes\Models\ReleaseNoteView;
use Ntriga\ReleaseNotes\Events\ReleaseNoteRetrieved;

class AddReleaseNoteView
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ReleaseNoteRetrieved $event
     * @return void
     */
    public function handle(ReleaseNoteRetrieved $event)
    {
        if($event->request->name === 'release-notes-panel') {
            $exists = ReleaseNoteView::where([
                'release_note_id' => $event->releaseNote->id,
                'user_id' => auth()->id(),
            ])
                                     ->exists();

            if (!$exists) {
                ReleaseNoteView::create([
                    'release_note_id' => $event->releaseNote->id,
                    'user_id' => auth()->id(),
                ]);
            }
        }
    }
}
