<div>
    <button type="button" class="btn btn-link" wire:poll.60s="getCount" id="release_notes_badge" wire:click="$emit('slideOut')">
        <i class="icofont-news"></i>
        @if($this->count > 0)
            <span class="badge badge-secondary animated swing">
                {{ $this->count }}
            </span>
        @endif
    </button>
</div>
