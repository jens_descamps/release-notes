@foreach($data as $type => $content)
    @if($type === 'IMG')
        <img src="{{ $content }}" alt="" class="img-fluid">
    @elseif($type === '#text')
        <p>
            {{ $content }}
        </p>
    @endif
@endforeach