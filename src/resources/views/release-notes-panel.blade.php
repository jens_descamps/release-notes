<div>
    @if($this->isOpen)
        <main class="col-md-4 col-lg-3 position-fixed bg-dark text-light shadow-lg pb-5 pl-5 pr-5 animated slideInRight" id="release_notes" style="height: 100vh; overflow-y: scroll; top: 0; right: 0;">
            <div class="modal-header">
                <h4 class="modal-title">
                    Updates
                </h4>
                <button type="button" class="close text-light" aria-label="Close" wire:click="$emit('slideOut')">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            @foreach($this->releaseNotes['data'] as $i => $releaseNote)
                <div class="modal-body">
                    <div class="border-bottom border-light">
                        <h5>
                            {{ $releaseNote['title'] }}
                        </h5>
                        <small>
                            {{ $releaseNote['updated_at'] }}
                        </small>
                        @if(isset($releaseNote['body']['blocks']))
                            @foreach($releaseNote['body']['blocks'] as $i => $block)
                                @include("release_notes::blocks.{$block['type']}", ['data' => $block['data']])
                            @endforeach
                        @else
                            {{ $releaseNote['body'] }}
                        @endif
                    </div>
                </div>
            @endforeach
            @if($this->releaseNotes['next_page_url'])
                <div class="text-center">
                    <button type="button" class="btn btn-link text-light text-lg" wire:click="loadMore({{ $this->releaseNotes['current_page'] }})">
                        Meer...
                    </button>
                </div>
            @endif
        </main>
    @endif
</div>
