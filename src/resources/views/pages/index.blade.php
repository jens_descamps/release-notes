@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="mb-4">
            <div class="line-bottom pb-2">
                <span class="bigtitle">
                    {{ __('Release Notes') }}
                </span>
                <div class="float-right">
                    <a href="{{route('release_notes.create')}}" class="btn btn-primary" title="{{ __('Add release note') }}">
                        <i class="icofont-plus"></i>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-12 my-3">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table mb-0">
                                    <thead>
                                    <tr>
                                        <th>
                                            {{ __('Title') }}
                                        </th>
                                        <th>
                                            {{ __('Last update') }}
                                        </th>
                                        <th class="text-right">
                                            {{ __('Actions') }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($releaseNotes as $i => $releaseNote)
                                        <tr>
                                            <td>
                                                {{ $releaseNote->title }}
                                            </td>
                                            <td>
                                                {{ $releaseNote->updated_at }}
                                            </td>
                                            <td class="text-right">
                                            <span class="table-mini-buttons">
                                                <a href="{{ route('release_notes.edit', ['releaseNote' => $releaseNote->id]) }}">
                                                    <i class="icofont-edit-alt"></i>
                                                </a>
                                                <button-delete-component
                                                        url="{{ route('release_notes.destroy', ['releaseNote' => $releaseNote]) }}"
                                                        v-on:delete="this.location.reload()">
                                                </button-delete-component>
                                            </span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $releaseNotes->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection