@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="mb-4">
            <release-notes-editor model="{{ json_encode($releaseNote) }}"></release-notes-editor>
        </section>
    </div>
@endsection