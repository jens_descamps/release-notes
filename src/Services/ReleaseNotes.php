<?php


namespace Ntriga\ReleaseNotes\Services;

use Ntriga\ReleaseNotes\Models\ReleaseNote;
use Illuminate\Filesystem\Filesystem;

class ReleaseNotes
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * ReleaseNotes constructor.
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param null $limit
     * @return array
     */
    public function get($limit = null)
    {
        $limit = $limit ?? config('release_notes.limit', 1);

        $releaseNotes = ReleaseNote::orderBy('id', 'DESC')
                                   ->where('active', true)
                                   ->simplePaginate($limit)
                                   ->toArray();

        return $releaseNotes;

        $releaseNotes = $this->getFiles();

        $response = [];

        foreach (array_reverse($releaseNotes) as $i => $releaseNote) {
            $markdown = file_get_contents($releaseNote);
            $response[] = app(\Parsedown::class)->setSafeMode(true)->text($markdown);
        }

        return $response;
    }

    /**
     * @return mixed
     */
    public function count()
    {
        $weeks = config('release_notes.weeks', 2);
        return ReleaseNote::orderBy('id', 'DESC')
                          ->doesntHave('views')
                          ->where('active', true)
                          ->whereDate('updated_at', '>=', now()->subWeeks($weeks))
                          ->count();
    }

    /**
     * @return array
     */
    private function getFiles(): array
    {
        $files = resource_path() . '/release_notes/*.md';
        return $this->filesystem->glob($files);
    }
}
