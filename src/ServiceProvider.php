<?php

namespace Ntriga\ReleaseNotes;

use Livewire\Livewire;
use Illuminate\Support\Facades\Event;
use Ntriga\ReleaseNotes\Livewire\ReleaseNotes;
use Ntriga\ReleaseNotes\Livewire\ReleaseNotesPanel;
use Ntriga\ReleaseNotes\Events\ReleaseNoteRetrieved;
use Ntriga\ReleaseNotes\Listeners\AddReleaseNoteView;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * 
     */
    public function boot(): void
    {
        $this->registerViews();
//        $this->registerAssets();
        $this->registerConfig();
        $this->registerEvents();
        $this->registerMigrations();
        $this->registerLivewireComponents();
    }

    /**
     *
     */
    protected function registerViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'release_notes');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/resources/views' => $this->app->resourcePath('views/vendor/release_notes'),
            ], 'release_notes');
        }
    }

    /**
     *
     */
    protected function registerConfig(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/release_notes.php', 'release_notes');
    }

    /**
     *
     */
    protected function registerEvents(): void
    {
        Event::listen(ReleaseNoteRetrieved::class, AddReleaseNoteView::class);
    }

    /**
     *
     */
    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    /**
     *
     */
    protected function registerLivewireComponents(): void
    {
        Livewire::component('release-notes-panel', ReleaseNotesPanel::class);
        Livewire::component('release-notes', ReleaseNotes::class);
    }

    /**
     *
     */
    private function registerAssets(): void
    {
        $this->publishes([
            __DIR__ . '/../public/js' => public_path('vendor/ntriga'),
        ], 'public');
    }
}