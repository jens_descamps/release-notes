<?php


namespace Ntriga\ReleaseNotes\Repositories;

use Ntriga\ReleaseNotes\Models\ReleaseNote;

/**
 * Class ReleaseNotesRepository
 * @package Ntriga\ReleaseNotes\Repositories
 */
class ReleaseNotesRepository
{
    /**
     * @param bool $paginated
     * @param int $perPage
     * @return mixed
     */
    public function index($paginated = true, $perPage = 10)
    {
        return ReleaseNote::orderBy('id', 'DESC')
                          ->select(['id', 'title', 'updated_at'])
                          ->simplePaginate($perPage);
    }
}