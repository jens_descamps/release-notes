<?php

namespace Ntriga\ReleaseNotes\Models;

use Illuminate\Database\Eloquent\Model;

class ReleaseNoteView extends Model
{
    protected $table = 'release_note_views';

    protected $fillable = [
        'release_note_id',
        'user_id',
    ];
}
