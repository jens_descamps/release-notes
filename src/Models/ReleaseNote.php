<?php

namespace Ntriga\ReleaseNotes\Models;

use Ntriga\ReleaseNotes\Events\ReleaseNoteRetrieved;
use Illuminate\Database\Eloquent\Model;

class ReleaseNote extends Model
{
    protected $fillable = [
        'title',
        'body',
        'active',
    ];

    protected $casts = [
        'body' => 'json',
    ];

    protected $dispatchesEvents = [
        'retrieved' => ReleaseNoteRetrieved::class,
    ];

    protected $with = [
        'views',
    ];

    /**
     *
     */
    public function views(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ReleaseNoteView::class)
            ->where('user_id', auth()->id());
    }
}
