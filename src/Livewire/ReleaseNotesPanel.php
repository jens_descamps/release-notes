<?php

namespace Ntriga\ReleaseNotes\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Ntriga\ReleaseNotes\Services\ReleaseNotes as ReleaseNotesService;

/**
 * Class ReleaseNotesPanel
 * @package App\Http\Livewire
 */
class ReleaseNotesPanel extends Component
{
    /**
     * @var array
     */
    public $releaseNotes = [];

    /**
     * @var int
     */
    public $releaseNotesCount = 0;

    /**
     * @var array
     */
    protected $listeners = [
        'slideOut' => 'toggle',
    ];

    /**
     * @var bool
     */
    public $isOpen = false;

    /**
     * Open/Close the panel
     */
    public function toggle(): void
    {
        $this->isOpen = !$this->isOpen;

        if ($this->isOpen) {
            $this->getReleaseNotes();
        }
    }

    /**
     * Load the next page of Release Notes,
     * prepending the 'previous' Release Notes to simulate the Load More functionality.
     *
     * @param $page
     */
    public function loadMore($page): void
    {
        $nextPage = $page + 1;

        Paginator::currentPageResolver(function () use ($nextPage) {
            return $nextPage;
        });

        $service = app(ReleaseNotesService::class);

        $previousReleaseNotes = $this->releaseNotes;
        $this->releaseNotes = $service->get();

        $this->releaseNotes['data'] = array_merge($previousReleaseNotes['data'], $this->releaseNotes['data']);
    }

    /**
     * @param null $limit
     */
    public function getReleaseNotes($limit = null): void
    {
        $service = app(ReleaseNotesService::class);

        $this->releaseNotes = $service->get($limit);
    }

    /**
     *
     */
    public function getReleaseNotesCount(): void
    {
        $service = app(ReleaseNotesService::class);

        $this->releaseNotesCount = $service->count();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        return view('release_notes::release-notes-panel');
    }
}
