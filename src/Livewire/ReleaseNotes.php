<?php

namespace Ntriga\ReleaseNotes\Livewire;

use Livewire\Component;
use Ntriga\ReleaseNotes\Services\ReleaseNotes as ReleaseNotesService;

class ReleaseNotes extends Component
{
    public $count = 0;

    /**
     * @var array
     */
    protected $listeners = [
        'slideOut' => 'getCount'
    ];

    /**
     * @return void
     */
    public function getCount() {
        $service = app(ReleaseNotesService::class);
        $this->count = $service->count();
    }

    /**
     *
     */
    public function slideOut()
    {
        $this->emit('slideOut', ['open' => true]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        $this->getCount();
        return view('release_notes::release-notes');
    }
}
