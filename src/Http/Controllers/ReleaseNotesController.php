<?php


namespace Ntriga\ReleaseNotes\Http\Controllers;


use Illuminate\Http\Request;
use Ntriga\ReleaseNotes\Models\ReleaseNote;
use Ntriga\ReleaseNotes\Repositories\ReleaseNotesRepository;

class ReleaseNotesController
{
    /**
     * @var ReleaseNotesRepository
     */
    private $repo;

    /**
     * ReleaseNotesController constructor.
     * @param ReleaseNotesRepository $repo
     */
    public function __construct(ReleaseNotesRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $releaseNotes = $this->repo->index();

        return view('release_notes::pages.index')
            ->with([
                'releaseNotes' => $releaseNotes,
            ]);
    }

    public function create()
    {
        return view('release_notes::pages.edit')
            ->with([
                'releaseNote' => new ReleaseNote(),
            ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'title',
            'body',
        ]);

        return ReleaseNote::create($data);
    }

    /**
     * @param ReleaseNote $releaseNote
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ReleaseNote $releaseNote)
    {
        return view('release_notes::pages.edit')
            ->with([
                'releaseNote' => $releaseNote,
            ]);
    }

    /**
     * @param ReleaseNote $releaseNote
     * @param Request $request
     * @return ReleaseNote
     */
    public function update(ReleaseNote $releaseNote, Request $request): ReleaseNote
    {
        $data = $request->only([
            'title',
            'body',
        ]);

        $releaseNote->update($data);

        return $releaseNote;
    }


    /**
     * @param ReleaseNote $releaseNote
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(ReleaseNote $releaseNote): \Illuminate\Http\JsonResponse
    {
        $releaseNote->delete();

        return response()->json();
    }
}