<?php

return [
    // Number of Release Notes to load at a time in the Panel.
    'limit' => 2,
    // Number of Weeks before the 'new' badge cut-off.
    'weeks' => 2,
];