let mix = require('laravel-mix');

mix.js('./src/resources/assets/js/release_notes.js', './public/js')
    .version();
